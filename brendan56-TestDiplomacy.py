from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):

    # -----
    # solve
    # -----
	
	def test_solve(self):
		r = StringIO("A Madrid Hold\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
            w.getvalue(), "A Madrid\n")

	def test_solve_2(self):
		r = StringIO("A Madrid Hold \nB Barcelona Move Madrid \nC London Support B\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

	def test_solve_3(self):
		r = StringIO("A Madrid Hold \n  B Barcelona Move Madrid\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

	def test_solve_4(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

	def test_solve_5(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

	def test_solve_6(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
	def test_solve_7(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A \n  \n \n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

	def test_solve_8(self):

		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Barcelona\n")

	def test_solve_9(self):

		r = StringIO("		A Madrid Support C\nB Barcelona Hold\nC London Move Madrid\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
            w.getvalue(), "A [dead]\nB Barcelona\nC [dead]\n")

	def test_solve_10(self):

		r = StringIO("		C London Move Madrid\nB Barcelona Hold\nA Madrid Support C\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
            w.getvalue(), "A [dead]\nB Barcelona\nC [dead]\n")

	def test_solve_11(self):

		r = StringIO("A Madrid Support C\nC Barcelona Hold\nB London Move Madrid\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Barcelona\n")

	def test_solve_12(self):

		r = StringIO("A Madrid Support C\nC Barcelona Support A\nB London Move Madrid\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC Barcelona\n")



if __name__ == "__main__":
    main()

	